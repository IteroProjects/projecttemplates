
var _SERVER_URL = "";

function retrieveJSON (server, requestParameters, onSuccess, onError) {
	$.ajaxSetup({
		'timeout' : 20000,
		'type': 'POST',
		global: false,
		'error' : function(XMLHttpRequest, textStatus, errorThrown) {
			var errorCode;
			if (textStatus == "timeout") {
				errorCode = -1;
			} else if (textStatus == "error") {
				errorCode = -2;
			} else if (textStatus == "abort") {
				errorCode = -3;
			} else if (textStatus == "parsererror") {
				errorCode = -4;
			}
			dataOnError(onError, errorCode, "Error occured while invoking WS - "+errorThrown);
		}
	});

	$.post(server, requestParameters, function(jsonResponse) {
		//console.log("JSON retrieved with success");
		
		// dirty dirty hack! remove when WS will be fixed
		/*var index = jsonResponse.indexOf("}}"); 
		if (index != -1){
			jsonResponse = jsonResponse.substring(0, (index+2));
			console.log("jsonResponse="+jsonResponse);
		}*/
		
		if (onSuccess) {
			try {
				onSuccess($.parseJSON(jsonResponse));
			}
			catch (e){
				dataOnError(onError, -15, "Error parsing WS response");
			}
		}
	});
}

/*
function wsRegisterUser(data, onSuccess, onError){
	var requestFull = formatRequestXML(data);
	requestParameters = {REQUEST_TYPE: "reguser", REQUEST: requestFull};
	retrieveJSON(_SERVER_URL, requestParameters, function(response){
		//console.log("response="+response.RESPONSE);
		var successCode = response.RESPONSE.ATSAKYMO_KODAS;
		if (onSuccess){
			onSuccess(successCode, response.RESPONSE);
		}
	}, onError);
}*/