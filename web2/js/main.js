if (getOsName() == "blackberry"){
	$(document).ready(function() {
		libsLoaded();
	}, false);
}
else {
	//document.addEventListener("deviceready", function () {
		$(document).ready(function() {
			libsLoaded();
		}, false);
	//}, false);
}

function libsLoaded(){
	var currentLanguage = LocalStor.getItem(LocalStor.LANGUAGE);
	if (currentLanguage == null){
		currentLanguage = _LANGUAGES.LITHUANIAN;
	}
	changeLang(currentLanguage);
    $.extend( $.mobile, {
        loadingMessage: ""
    });
}

function hackUI(){
	//$(".search-input").parent().css("width", "100%");
}

function exitOnBack(){
	/*console.log("Back button pressed");
	if ((_currentPage == _PAGES.HOME) || (_currentPage == _PAGES.TERMS) || (_currentPage == _PAGES.SEARCH)){
		if (getOsName() == "android"){
			navigator.app.exitApp();
		}
		else if (getOsName() == "blackberry"){
			blackberry.app.exit();
		}
	}
	else {
		$.mobile.changePage($("#" + _previousPage), {transition: "slide",reverse: true});
	}*/
}

function releaseBackButton(){
	if (getOsName() == "blackberry"){
		blackberry.system.event.onHardwareKey(blackberry.system.event.KEY_BACK,null);  
	}
	else {
		document.removeEventListener("backbutton", exitOnBack, false);
	}
}

function captureBackButton(){
	if (getOsName() == "blackberry"){
		blackberry.system.event.onHardwareKey(blackberry.system.event.KEY_BACK,exitOnBack);  
	}
	else {
		document.addEventListener("backbutton", exitOnBack, false);
	}
}

var _PAGES = {TERMS:"terms-page", HOME:"home-page", SPLASHSCREEN:"splashscreen-page"};

$(document).bind("mobileinit", function () {
	jQuery.support.cors = true;
	hackUI();
	//$.mobile.touchOverflowEnabled = true;
	$.mobile.defaultPageTransition = 'none';
    $.mobile.defaultDialogTransition = 'none';
    // $.mobile.useFastClick = true;
	
    setTimeout(function(){
		var firstPage = null;
		if (LocalStor.getItem(LocalStor.ACCEPTED_RULES) != true){
    		firstPage = _PAGES.TERMS;
		}
		else {
    		firstPage = _PAGES.HOME;
		}
		//firstPage = _PAGES.SPLASHSCREEN;
        $.mobile.changePage($("#"+firstPage));
        
		if (getOsName() == "windows"){
			document.onselectstart = function(){return false;}; // disables text selection on WP
        }
    }, 200);

});

function showLoading(){
	$("body").append('<div class="modalWindow"/>');
	$.mobile.showPageLoadingMsg();
}

function hideLoading(){
	$.mobile.hidePageLoadingMsg();
	$(".modalWindow").remove();
}


var _currentPage = null;
var _previousPage = null;

$(document).bind("pagechange", function( event, data ){
	var pageName = (typeof data.toPage !== "string") ? data.toPage[0].id : data.toPage;
	_previousPage = _currentPage;
	_currentPage = pageName;

});

function changeLang(newLanguage){
	changeLanguage(newLanguage);
	loadTermsText();
	LocalStor.setItem(LocalStor.LANGUAGE, newLanguage);
}

function loadTermsText(){
	var osName = getOsName();
	console.log("osName="+osName);
	var prefix = "";
	if (osName == "windows"){
		//prefix = "/app/www/assets/terms/";
		prefix = "assets/terms/";
	}
	else if (osName == "blackberry"){
		prefix = "assets/terms/";
	}
	else if (osName == "android"){
		prefix = "./assets/terms/";
	}
	var fileName;
	//var language = getLanguage();
	fileName = "lithuanian.txt";
	console.log(prefix+fileName);
    $.get(prefix+fileName, function(data) {
    	$('.terms-content').html(data);
    });
}