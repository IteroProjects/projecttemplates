var JSONLib = {};

JSONLib.stringify = function (obj) {  
    var t = typeof (obj);  
    if (t != "object" || obj === null) {  
        // simple data type  
        if (t == "string") obj = '"'+obj+'"';  
        return String(obj);  
    }  
    else {  
        // recurse array or object  
        var n, v, json = [], arr = (obj && obj.constructor == Array);  
        for (n in obj) {  
            v = obj[n]; t = typeof(v);  
            if (t == "string") v = '"'+v+'"';  
            else if (t == "object" && v !== null) v = JSONLib.stringify(v);  
            json.push((arr ? "" : '"' + n + '":') + String(v));  
        }  
        return (arr ? "[" : "{") + String(json) + (arr ? "]" : "}");  
    }  
};

// implement JSON.parse de-serialization  
JSONLib.parse = function (str) {
    if (str === "") str = '""';
    eval("var p=" + str + ";");
    return p;  
};

var LocalStor = {};

LocalStor.UUID = "UUID";
LocalStor.ACCEPTED_RULES = "ACCEPTED_RULES";
LocalStor.LANGUAGE = "LANGUAGE";

LocalStor.setItem = function(key, obj){
	if ((key != null) && (obj != null)){
		var stringifiedObj = JSONLib.stringify(obj);
		if (stringifiedObj && window.localStorage){
			window.localStorage.setItem(key,stringifiedObj);
		}
	}
}

LocalStor.getItem = function(key){
	var result = null;
	if (key != null && window.localStorage != null){
		var serializedItem = window.localStorage.getItem(key);
		if (serializedItem != null && serializedItem != ""){
			var obj = JSONLib.parse(serializedItem);
			if (obj){
				result = obj;
			}
		}
	}
	return result;
}

LocalStor.clear = function(){
	if (window.localStorage){
		// TODO FIX
		//window.localStorage.removeItem("searchedContacts");
	}
}

var _LANGUAGES = {LITHUANIAN:"lt"};

// possible values - android, ios, windows
function getOsName(){
 	var browserVersion = navigator.appVersion.toLowerCase();
	//console.log("browserVersion="+browserVersion);
	var androidName = "android";
	var windowsName = "windows";
	var blackberryName = "blackberry";
	if (browserVersion.indexOf(androidName) != -1){
		return 'android';
	}
	else if (browserVersion.indexOf(windowsName) != -1){
		return 'windows';
	}
	else if (browserVersion.indexOf(blackberryName) != -1){
		return 'blackberry';
	}
}

var _userLang = null;
function getLanguage(){
	/*if (getOsName() != 'android'){
		if (getOsName() == 'windows'){
			userLang = navigator.systemLanguage;
		}
		else {
			userLang = (navigator.language) ? navigator.language : navigator.userLanguage;
		}
	}
	else {
		var browserInfo = navigator.appVersion.toLowerCase();
		var index = browserInfo.indexOf("lt-");
		if (index != -1){
			userLang = browserInfo.slice(index,index+3);
		}
		else {
			index = browserInfo.indexOf("en-");
			if (index != -1){
				userLang = browserInfo.slice(index,index+3);
			}
		}
	}
	if (getOsName() != 'android'){
		if (getOsName() == 'windows'){
			userLang = navigator.systemLanguage;
		}
		else {
			userLang = (navigator.language) ? navigator.language : navigator.userLanguage;
		}
	}
	else {
		var browserInfo = navigator.appVersion.toLowerCase();
		var index = browserInfo.indexOf("lt-");
		if (index != -1){
			userLang = browserInfo.slice(index,index+3);
		}
		else {
			index = browserInfo.indexOf("en-");
			if (index != -1){
				userLang = browserInfo.slice(index,index+3);
			}
		}
	}
	if (userLang != null){
		userLang = userLang.slice(0,2);
	}*/
	return _userLang;
}

function getLanguageObj(){
	var userLang = getLanguage();
	if (userLang == _LANGUAGES.LITHUANIAN){
		return _lt;
	}
	else {
		return _lt;
	}
}

var _translations = null;

function changeLanguage(newLanguage){
	console.log("newLanguage="+newLanguage);
	if (newLanguage == _LANGUAGES.LITHUANIAN){
		_userLang = _LANGUAGES.LITHUANIAN;
	}
	else {
		_userLang = _LANGUAGES.LITHUANIAN;
	}
	updateHtmlLanguage();
}

function t(key){
	_translations = getLanguageObj();
	if (_translations[key] == null){
		return "undefined key: " + key;
	}
	else {
		return _translations[key];
	}
}

function updateHtmlLanguage(){
	$(".translate").each(function(i, obj){
		if ($(this).attr("translate-key")) {
			var $child = $(this).find(".ui-btn-text");
			if ($child.length){
				$child.text(t($(this).attr("translate-key")));
			}
			else {
				$(this).text(t($(this).attr("translate-key")));
			}
		}
	});

	/*
	// hack for login button
	var counter = $(".login-button .ui-btn-text").size();
	var $loginButton = $(".login-button");
	if (counter > 0){
		$loginButton.find(".ui-btn-text").text(t("login.submit"));
	}
	else {
		$loginButton.text(t("login.submit"));
	}
	*/
}

function showAlertDialog(message, title, buttonText){
	console.log("Error occured. Error message = " + message);
	if (title == null){
		title = t("general.popup_error_title");
	}
	if (buttonText == null){
		buttonText = t("general.popup_error_button")
	}
	if (getOsName() == "blackberry"){
		alert(message);
	}
	else {
		navigator.notification.alert(
				message,
			    function(){},
			    title,
			    buttonText
			);
	}

}
