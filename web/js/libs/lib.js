var JSONLib = {};

JSONLib.stringify = function (obj) {  
    var t = typeof (obj);  
    if (t != "object" || obj === null) {  
        // simple data type  
        if (t == "string") obj = '"'+obj+'"';  
        return String(obj);  
    }  
    else {  
        // recurse array or object  
        var n, v, json = [], arr = (obj && obj.constructor == Array);  
        for (n in obj) {  
            v = obj[n]; t = typeof(v);  
            if (t == "string") v = '"'+v+'"';  
            else if (t == "object" && v !== null) v = JSONLib.stringify(v);  
            json.push((arr ? "" : '"' + n + '":') + String(v));  
        }  
        return (arr ? "[" : "{") + String(json) + (arr ? "]" : "}");  
    }  
};

// implement JSON.parse de-serialization  
JSONLib.parse = function (str) {
    if (str === "") str = '""';
    eval("var p=" + str + ";");
    return p;  
};

var localStor = {};

localStor.setItem = function(key, obj){
	if ((key != null) && (obj != null)){
		var stringifiedObj = JSONLib.stringify(obj);
		if (stringifiedObj && window.localStorage){
			window.localStorage.setItem(key,stringifiedObj);
		}
	}
}

localStor.getItem = function(key){
	var result = null;
	if (key != null && window.localStorage != null){
		var serializedItem = window.localStorage.getItem(key);
		if (serializedItem != null && serializedItem != ""){
			var obj = JSONLib.parse(serializedItem);
			if (obj){
				result = obj;
			}
		}
	}
	return result;
}

localStor.clear = function(){
	if (window.localStorage){
		window.localStorage.removeItem("searchedContacts");
		window.localStorage.removeItem("statuses");
		window.localStorage.removeItem("confRooms");
		window.localStorage.removeItem("contacts");
		window.localStorage.removeItem("username");
		window.localStorage.removeItem("password");
		window.localStorage.removeItem("config");
	}
}

var _LANGUAGES = {FINNISH:"fi", ENGLISH:"en"};

// possible values - android, ios, windows
function getOsName(){
 	var browserVersion = navigator.appVersion.toLowerCase();
	var androidName = "android";
	var windowsName = "windows";
	if (browserVersion.indexOf(androidName) != -1){
		return 'android';
	}
	else if (browserVersion.indexOf(windowsName) != -1){
		return 'windows';
	}
	else {
		return 'ios';
	}
}

function getLanguage(){
	var userLang = null;
	if (getOsName() != 'android'){
		if (getOsName() == 'windows'){
			userLang = navigator.systemLanguage;
		}
		else {
			userLang = (navigator.language) ? navigator.language : navigator.userLanguage;
		}
	}
	else {
		var browserInfo = navigator.appVersion.toLowerCase();
		var index = browserInfo.indexOf("fi-");
		if (index != -1){
			userLang = browserInfo.slice(index,index+3);
		}
		else {
			index = browserInfo.indexOf("en-");
			if (index != -1){
				userLang = browserInfo.slice(index,index+3);
			}
		}
	}
	if (getOsName() != 'android'){
		if (getOsName() == 'windows'){
			userLang = navigator.systemLanguage;
		}
		else {
			userLang = (navigator.language) ? navigator.language : navigator.userLanguage;
		}
	}
	else {
		var browserInfo = navigator.appVersion.toLowerCase();
		var index = browserInfo.indexOf("fi-");
		if (index != -1){
			userLang = browserInfo.slice(index,index+3);
		}
		else {
			index = browserInfo.indexOf("en-");
			if (index != -1){
				userLang = browserInfo.slice(index,index+3);
			}
		}
	}
	if (userLang != null){
		userLang = userLang.slice(0,2);
	}
	return userLang;
}

function getLanguageObj(){
	var userLang = getLanguage();
	if (userLang == _LANGUAGES.FINNISH){
		return _fi;
	}
	else {
		return _en;
	}
}

var _translations = null;

function t(key){
	if(_translations == null){
		_translations = getLanguageObj();
	}
	if (_translations[key] == null){
		return "undefined key: " + key;
	}
	else {
		return _translations[key];
	}
}

function updateHtmlLanguage(){
	$(".translate").each(function(i, obj){
		if ($(this).attr("translate-key")) {
			this.innerHTML = t($(this).attr("translate-key"));
		}
	});

	/*
	// hack for login button
	var counter = $(".login-button .ui-btn-text").size();
	var $loginButton = $(".login-button");
	if (counter > 0){
		$loginButton.find(".ui-btn-text").text(t("login.submit"));
	}
	else {
		$loginButton.text(t("login.submit"));
	}
	*/
}

function showAlertDialog(message, title){
	console.log("Error occured. Error message = " + message);
	if (title == null){
		title = t("general.popup_error_title");
	}
	navigator.notification.alert(
		message,  // message
	    function(){},         // callback
	    title                 // buttonName
	);

}
