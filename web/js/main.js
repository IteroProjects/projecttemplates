//document.addEventListener("deviceready", function () {
	$(document).ready(function() {
		libsLoaded();
	}, false);
//});

function libsLoaded(){
	updateHtmlLanguage();
}

function exitOnBack(){
	console.log("Back Button Pressed!");
	navigator.app.exitApp();
}

function captureBackButton(){
    document.addEventListener("backbutton", exitOnBack, false);
}

$(document).bind("mobileinit", function () {
	//$.mobile.touchOverflowEnabled = true;
	$.mobile.defaultPageTransition = 'none';
    $.mobile.defaultDialogTransition = 'none';
    // $.mobile.useFastClick = true;
	
    setTimeout(function(){
        $.mobile.changePage($("#testPage"));
    }, 2000);
});

var _currentPage = null;
var _previousPage = null;

$(document).bind("pagebeforechange", function( event, data ){
	var pageName = (typeof data.toPage !== "string") ? data.toPage[0].id : data.toPage;
});

$(document).bind("pagechange", function( event, data ){
	var pageName = (typeof data.toPage !== "string") ? data.toPage[0].id : data.toPage;
	_previousPage = _currentPage;
	_currentPage = pageName;

});